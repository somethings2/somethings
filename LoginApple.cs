using AppleAuth;
using AppleAuth.Enums;
using AppleAuth.Interfaces;
using AppleAuth.Native;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Unity.Services.Authentication;
using Unity.Services.CloudSave;
using Unity.Services.Core;
using UnityEngine;
using UnityEngine.UI;

public class AppleManager : MonoBehaviour
{
    [SerializeField] private Text userName;
    IAppleAuthManager m_AppleAuthManager;
    public string Token { get; private set; }
    public string Error { get; private set; }
    public bool loginSuccess;
    int count = 0;
    async void Start()
    {
        await UnityServices.InitializeAsync();
    }
    private void Awake()
    {
        if (!PlayerPrefs.HasKey("countLogin"))
        {
            PlayerPrefs.SetInt("countLogin", 0);
        }
    }
    public void Initialize()

    {
        var deserializer = new PayloadDeserializer();
        m_AppleAuthManager = new AppleAuthManager(deserializer);
    }

    public void Update()
    {
        if (m_AppleAuthManager != null)
        {
            m_AppleAuthManager.Update();
        }
    }

    public void LoginToApple()
    {
        // Initialize the Apple Auth Manager
        if (m_AppleAuthManager == null)
        {
            Initialize();
        }

        // Set the login arguments
        var loginArgs = new AppleAuthLoginArgs(LoginOptions.IncludeEmail | LoginOptions.IncludeFullName);

        // Perform the login
        m_AppleAuthManager.LoginWithAppleId(
            loginArgs,
            credential =>
            {
                var appleIDCredential = credential as IAppleIDCredential;
                if (appleIDCredential != null)
                {
                    var idToken = Encoding.UTF8.GetString(
                        appleIDCredential.IdentityToken,    
                        0,
                        appleIDCredential.IdentityToken.Length);

                    // Retrieve the user's full name if available
                    string userFullName = "";
                    if (appleIDCredential.FullName != null)
                    {
                        userFullName = $"{appleIDCredential.FullName.GivenName} {appleIDCredential.FullName.FamilyName}";
                    }

                    Debug.Log("User name: " + userFullName);
                    Debug.Log("Sign-in with Apple successfully done. IDToken: " + idToken);
                    Token = idToken;
                    PlayerPrefs.SetString("userFullName", userFullName);
                    //userName.text = userFullName;
                    loginSuccess = true;

                    if (Token != null)
                    {
                        CallSignInWithAppleAsync(Token);
                    }


                    //if (AuthenticationService.Instance.PlayerId == null)
                    //{
                    //    SaveData();
                    //}
                    

                    //PlayerPrefs.SetInt("countLogin", PlayerPrefs.GetInt("countLogin")+1);
                    //if (PlayerPrefs.GetInt("countLogin") == 1)
                    //{
                    //    PlayerPrefs.SetString("loginUsername", userFullName);
                    //}




                    //DataController.instance.InitNewData(PlayerPrefs.GetString("loginUsername"));
                    //UIController.instance.login.SetActive(false);
                    //UIController.instance.loginPanel.SetActive(false);
                    //UIController.instance.userName.SetActive(true);
                    //PlayerPrefs.SetInt("LogIn", 1);
                }
                else
                {
                    loginSuccess = false;
                    Debug.Log("Sign-in with Apple error. Message: appleIDCredential is null");
                    Error = "Retrieving Apple Id Token failed.";
                }
            },
            error =>
            {
                Debug.Log("Sign-in with Apple error. Message: " + error);
                Error = "Retrieving Apple Id Token failed.";
            }
        );

        
    }

    async void CallSignInWithAppleAsync(string idToken)
    {
        UIController.instance.loggingin.SetActive(true);

        await SignInWithAppleAsync(idToken);
    }

    async Task SignInWithAppleAsync(string idToken)
    {
        try
        {
            await AuthenticationService.Instance.SignInWithAppleAsync(idToken);
            await LoadData();
            Debug.Log("SignIn is successful.");
            Debug.Log("Player ID: " + AuthenticationService.Instance.PlayerId);
            DataController.instance.InitNewData(PlayerPrefs.GetString("loginUsername"));
            UIController.instance.login.SetActive(false);
            UIController.instance.loggingin.SetActive(false);
            UIController.instance.loginPanel.SetActive(false);
            UIController.instance.userName.SetActive(true);
            PlayerPrefs.SetInt("LogIn", 1);
        }
        catch (AuthenticationException ex)
        {
            // Compare error code to AuthenticationErrorCodes
            // Notify the player with the proper error message
            Debug.LogException(ex);
        }
        catch (RequestFailedException ex)
        {
            // Compare error code to CommonErrorCodes
            // Notify the player with the proper error message
            Debug.LogException(ex);
        }
    }

    async void SaveData()
    {

        var data = new Dictionary<string, object> { { "userName", PlayerPrefs.GetString("userFullName")} };
        await CloudSaveService.Instance.Data.ForceSaveAsync(data);
    }


    async Task LoadData()
    {
        while (true)
        {
            Dictionary<string, string> serverData = await CloudSaveService.Instance.Data.LoadAsync(new HashSet<string> { "userName" });
            if (serverData != null && serverData.Count > 0)
            {
                if (serverData.ContainsKey("userName"))
                {
                    PlayerPrefs.SetString("loginUsername", serverData["userName"]);
                    Debug.Log("loginUsername " + PlayerPrefs.GetString("loginUsername"));
                }
                else
                {
                    Debug.Log("Has not key");
                }
                break;
            }
            else
            {
                SaveData();
                await Task.Delay(1000); 
            }
        }
    }

}
